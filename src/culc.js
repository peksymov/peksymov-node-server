function sum(a, b, ...rest) {
    return a + b + rest.reduce((acc,val) => acc + val, 0)
}

module.exports.sum = sum
